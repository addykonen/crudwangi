<?php

namespace App\Http\Controllers;

use Exception;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $context = [
            "users" => User::all(),
            "title" => "Semua User"
        ];

        return view("user.index",$context);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $context = [
            "title" => "Buat User"
        ];
        return view("user.create",$context);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if($request->password !== $request->confirm_password){
            return redirect()->back()->with('swal-fail-message','Konfirmasi Password Dan Password Harus Sama!');
        }

        try {
            User::create([
                "name" => $request->name,
                "password" => bcrypt($request->password),
                "email" => $request->email
            ]);
        }
        catch(Exception $fail)
        {
            var_dump($fail->getMessage());
            throw $fail;
        }

        return redirect()->back()->with('swal-success-message', 'Berhasil membuat User Baru!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(User $user)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(User $user)
    {
        $context = [
            "title" => "Edit $user->name",
            "user" => $user
        ];
        return view("user.edit", $context);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, User $user)
    {
        try {
            $user->update([
                "name" => $request->name,
                "email" => $request->email
            ]);
        }
        catch(Exception $fail){
            throw $fail;
        }

        return redirect()->back()->with("swal-success-message", "Berhasil Ubah Data Pengguna!");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $user)
    {
        $user->delete();
        return redirect()->back()->with("swal-success-message",'berhasil menghapus pengguna');
    }

    public function register()
    {
        $context = ['title' => 'Register Wangi'];
        return view("user.register",$context);
    }

    public function storeRegister(Request $request)
    {
        if($request->password !== $request->confirm_password){
            return redirect()->back()->with('swal-fail-message','Konfirmasi Password Dan Password Harus Sama!');
        }

        try {
            User::create([
                "name" => $request->name,
                "password" => bcrypt($request->password),
                "email" => $request->email
            ]);
        }
        catch(Exception $fail)
        {
            var_dump($fail->getMessage());
            throw $fail;
        }

        return redirect()->to(route('login'))->with('swal-success-message', 'selamat akun anda telah berhasil terdaftar, silahkan login sekarang');

    }

    public function login()
    {
        $context = ['title' => 'Login'];
        return view("user.login", $context);
    }

    public function authenticate(Request $request)
    {
        $credentials = $request->validate([
            'email' => ['required', 'email'],
            'password' => ['required'],
        ]);
 
        if (Auth::attempt($credentials)) {
            $request->session()->regenerate();
 
            return redirect()->intended('/user');
        }
 
        return back()->withErrors([
            'email' => 'Email Atau Password Salah.',
        ])->onlyInput('email');
    }

    public function logout(Request $request)
    {    
        Auth::logout();
    
        $request->session()->invalidate();
    
        $request->session()->regenerateToken();
    
        return redirect('/');
    }
}
