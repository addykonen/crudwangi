<?php

use App\Http\Controllers\UserController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::middleware("guest")->group(function(){
    Route::get("/user/register", [UserController::class, 'register'])->name('register');
    Route::get("/user/login", [UserController::class, 'login'])->name('login');
    Route::post("/user/store-register", [UserController::class, 'storeRegister'])->name('store-register');
    Route::post("/user/authenticate", [UserController::class, 'authenticate'])->name('authenticate');
});

Route::post('user/logout', [UserController::class, 'logout'])->name("logout")->middleware("auth");
Route::resource("/user", UserController::class)->middleware('auth');