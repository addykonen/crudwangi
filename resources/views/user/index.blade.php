@extends("layouts.app")

@section('content')

<div class="flex flex-col">
  <div>
    <p class="text-purple-600 text-xl font-bold">Tabel Pengguna</p> 
  </div>
  <div class="overflow-x-auto sm:-mx-6 lg:-mx-8">
    <div class="py-2 inline-block min-w-full sm:px-6 lg:px-8">
      <div class="overflow-hidden">
        <table class="min-w-full">
          <thead class="border-b">

            <tr class="bg-purple-400 text-white">
              <th scope="col" class="text-sm font-medium px-6 py-4 text-left">
                #
              </th>
              <th scope="col" class="text-sm font-medium px-6 py-4 text-left">
                Nama
              </th>
              <th scope="col" class="text-sm font-medium px-6 py-4 text-left">
                Email
              </th>
              <th scope="col" class="text-sm font-medium px-6 py-4 text-left">
                Aksi
              </th>
            </tr>
          </thead>
          <tbody>
            @foreach($users as $user)
            <tr class="border-b">
              <td class="px-6 py-4 whitespace-nowrap text-sm font-medium text-gray-900">{{ $loop->iteration }}</td>
              <td class="text-sm text-gray-900 font-light px-6 py-4 whitespace-nowrap capitalize">
                {{ $user->name }}
              </td>
              <td class="text-sm text-gray-900 font-light px-6 py-4 whitespace-nowrap capitalize">
               {{ $user->email }}
              </td>
              <td class="text-sm text-gray-900 font-light px-6 py-4 whitespace-nowrap capitalize">
                <div class="inline">
                  <a href="{{ route('user.edit',['user' => $user]) }}" class="btn rounded px-5 py-2 bg-purple-500 font-bold text-white">Edit</a>
                </div>
                <form action="{{ route('user.destroy',['user' => $user]) }}" method="post" class="inline del-btn">
                  @csrf
                  @method("DELETE")
                  <button type="button" class="del-btn rounded px-5 py-2 border-purple-500 font-bold text-purple-500 hover:bg-purple-500 hover:text-white">Hapus</button>
                </form>
              </td>
            </tr>
            @endforeach
          </tbody>
        </table>
      </div>
    </div>
  </div>
</div>
@endsection