@extends('layouts.app')

@section('content')
    <form action="{{ route('authenticate') }}" method="post" class="shadow p-5 w-8/12 mx-auto">
        <h1 class="text-3xl font-bold text-purple-500 my-3 text-center">Login CrudWangi</h1>
        @csrf
        <div class="grid grid-cols-1 gap-4 ">
            <div>
                <label class="block">
                    <span class="after:content-['*'] after:ml-0.5 after:text-red-500 block text-sm font-medium text-slate-700">
                        Email
                    </span>
                    <input type="email" value="{{ old('email','') }}" name="email" class="@error('email') border-red-500 @enderror  mt-1 px-3 py-2 bg-white border shadow-sm border-slate-300 placeholder-slate-400 focus:outline-none focus:border-purple-500 focus:ring-purple-500 block w-full rounded-md sm:text-sm focus:ring-1" placeholder="you@example.com" />
                    @error('email') 
                    <span class="text-red-500">{{ $message }}</span> @enderror
                </label>
            </div>
            <div>
                <label class="block">
                    <span class="after:content-['*'] after:ml-0.5 after:text-red-500 block text-sm font-medium text-slate-700">
                        Password
                    </span>
                    <input type="password" name="password" class="mt-1 px-3 py-2 bg-white border shadow-sm border-slate-300 placeholder-slate-400 focus:outline-none focus:border-purple-500 focus:ring-purple-500 block w-full rounded-md sm:text-sm focus:ring-1" required/>
                </label>
            </div>
           
            <div>
                <button class="px-5 py-1 bg-purple-500 rounded-sm text-white">Login</button>
            </div>
        </div>
        <div class="my-3 text-center">
            <span class="text-gray-500 ">Belum Mempunyai Akun? <a href="{{ route('register') }}" class="text-purple-500">Register Disini</a></span>
        </div>
    </form>
@endsection