@extends('layouts.app')

@section('content')
    <form action="{{ route('user.store') }}" method="post" class="shadow p-5 w-8/12 mx-auto">
        <h1 class="text-3xl font-bold text-purple-500 my-3 text-center">Buat User</h1>
        @csrf
        <div class="grid grid-cols-1 gap-4 ">
            <div>
                <label class="block">
                    <span class="after:content-['*'] after:ml-0.5 after:text-red-500 block text-sm font-medium text-slate-700">
                        Name
                    </span>
                        <input type="text" name="name" class="mt-1 px-3 py-2 bg-white border shadow-sm border-slate-300 placeholder-slate-400 focus:outline-none focus:border-purple-500 focus:ring-purple-500 block w-full rounded-md sm:text-sm focus:ring-1" placeholder="ex. Adam" />
                    </label>
            </div>
            <div>
                <label class="block">
                    <span class="after:content-['*'] after:ml-0.5 after:text-red-500 block text-sm font-medium text-slate-700">
                        Email
                    </span>
                    <input type="email" name="email" class="mt-1 px-3 py-2 bg-white border shadow-sm border-slate-300 placeholder-slate-400 focus:outline-none focus:border-purple-500 focus:ring-purple-500 block w-full rounded-md sm:text-sm focus:ring-1" placeholder="you@example.com" />
                </label>
            </div>
            <div>
                <label class="block">
                    <span class="after:content-['*'] after:ml-0.5 after:text-red-500 block text-sm font-medium text-slate-700">
                        Password
                    </span>
                    <input type="password" name="password" class="mt-1 px-3 py-2 bg-white border shadow-sm border-slate-300 placeholder-slate-400 focus:outline-none focus:border-purple-500 focus:ring-purple-500 block w-full rounded-md sm:text-sm focus:ring-1" required/>
                </label>
            </div>
            <div>
                <label class="block">
                    <span class="after:content-['*'] after:ml-0.5 after:text-red-500 block text-sm font-medium text-slate-700">
                        Konfirmasi Password
                    </span>
                    <input type="password" name="confirm_password" class="mt-1 px-3 py-2 bg-white border shadow-sm border-slate-300 placeholder-slate-400 focus:outline-none focus:border-purple-500 focus:ring-purple-500 block w-full rounded-md sm:text-sm focus:ring-1" required/>
                </label>
            </div>
            <div>
                <button class="px-5 py-1  bg-purple-500 rounded-sm text-white">Buat</button>
                 <a href="{{ route('user.index') }}" class="btn rounded px-5 py-2 border-purple-500 font-bold text-purple-500 hover:bg-purple-500 hover:text-white">Home</a>
            </div>
        </div>
    </form>
@endsection