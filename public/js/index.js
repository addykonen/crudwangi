document.addEventListener("click", function(e){
    console.log('halo')
    console.log(e.target)
    if(e.target.classList.contains("del-btn")){
        Swal.fire({
            title: 'Anda Yakin?',
            text: "Anda Tidak Bisa Mendapatkan Data Ini Kembali Setelah Terhapus",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes, delete it!'
        }).then((result) => {
            if (result.isConfirmed) {
                e.target.parentElement.submit();
            }
            else {
                Swal.fire('Huh Data Ini Tidak jadi terhapus!');
            }
    })
    }
})