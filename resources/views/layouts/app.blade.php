<!doctype html>
<html>
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  @vite('resources/css/app.css')
  <title class="capitalize">{{ $title ?? 'page' }}</title>
  <script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>
</head>
<body>
    <div id="message" swal-success-message="{{ session("swal-success-message") }}" swal-fail-message="{{ session("swal-fail-message") }}">
        @if(session()->has("swal-success-message"))
          <script>
            Swal.fire(
              "Berhasil",
              document.getElementById('message').getAttribute("swal-success-message"),
              "success"
            );
          </script>
        @elseif(session()->has("swal-fail-message"))
        <script>
            Swal.fire(
              "Oops",
              document.getElementById('message').getAttribute("swal-fail-message"),
              "error"
            );
          </script>
        @endif
    </div>
    <nav class="p-4 flex align-middle justify-between bg-purple-600 text-white">
        <div class="px-5">
            <a href="{{ route('user.index') }}" class="font-bold text-2xl">CRUD WANGI</a>
        </div>
        <div class="">
            <a href="{{ route('user.index') }}" class="text-md md:mx-2">Home</a>
            <a href="{{ route('user.create') }}" class="text-md md:mx-2">Buat User</a>
            @auth
            <form action="{{ route('logout') }}" method="post" class="inline">
              @csrf
              <button class="text-md md:mx-2">Logout</button> 
            </form>
            @endauth
            @guest
            <a href="{{ route('login') }}" class="text-md md:mx-2">Login</a>   
            @endguest

        </div>
    </nav>

    <div class="md:px-10 md:py-5">
        @yield('content')
    </div>
    @yield('js')
    <script src="{{ url("js/index.js") }}"></script>
</body>
</html>